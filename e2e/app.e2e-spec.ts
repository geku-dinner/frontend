import { GeKuDinnerPage } from './app.po';

describe('ge-ku-dinner App', () => {
  let page: GeKuDinnerPage;

  beforeEach(() => {
    page = new GeKuDinnerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
