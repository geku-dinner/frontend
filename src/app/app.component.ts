import { Component } from '@angular/core';

import { Restangular } from 'ngx-restangular';

import * as _ from 'lodash';
import * as moment from 'moment';

export enum eventRole {
  cook,
  participant,
  none
}

moment.locale('de');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  
  title = 'app';

  events = [];
  users = [];

  newUser = {
    name: ''
  };

  _ = _;

  constructor(private restangular: Restangular) {
    
  }

  ngOnInit() {
    this.loadUsers();
    this.loadEvents();
  }

  loadUsers() {
    this.restangular.all('users').getList().subscribe( users => this.users = users);
  }

  changeWeek(diff) {
    let currWeek = _.minBy(this.events, 'date');
    this.loadEvents( moment(currWeek.date).add(diff, 'week') );
  }

  loadEvents(minDate = moment().startOf('week')) {
    
    let maxDate = moment(minDate).endOf('week');

    this.restangular
      .all('events')
      .getList({ from: minDate.format('YYYY-MM-DD'), to: maxDate.format('YYYY-MM-DD') })
      .subscribe( events => {

        for (let currDate = minDate; currDate.isBefore(maxDate); currDate.add(1, 'day')) {
          if (currDate.isoWeekday() == 6 || currDate.isoWeekday() == 7)
            break;

          if ( _.map(events, event => event['date']).indexOf(currDate.format('YYYY-MM-DD')) == -1 ) {
            let currEvent = this.restangular.one('events');
            currEvent.date = currDate.format('YYYY-MM-DD');
            events.push(currEvent);
          }
        }

        this.events = _.sortBy(events, 'date');
      });

    
  }

  createUser() {
    this.restangular.all('users').post(this.newUser).subscribe( newUser => {
      this.users.push(newUser);
      this.newUser = { name: '' };
    });
  }

  changeState(event, _user) {
    let currState = parseInt( eventRole[event.getRole(_user)] );
    let nextState = (currState + 1) % 3;

    if ( nextState == eventRole.cook && event.hasCook() ) {
      // In case there is a cook defined already, we want to skip that state
      nextState = nextState + 1;
    } 
    
    switch (nextState) {
      case eventRole.cook: 
        console.log('Going to setCook()');
        this.setCook(event, _user);
        break;
      case eventRole.participant:
        console.log('Goint to addParticipant()');
        this.addParticipant(event, _user);

        if ( event.isCook(_user) ) {
          // In case the new participant has previously be defined as cook, we want to remove him
          this.removeCook(event);
          console.log('Goint to removeCook()');
        }

        break;
      case eventRole.none:
        console.log('Goint to removeParticipant()');
        this.removeParticipant(event, _user);
        break;
      default:
        console.log('Couldn\'t find any suitable method to transfer to the next state.');
    }
  }

  addParticipant(event, _user) {
    event.addParticipant(_user).subscribe( newEvent => {this.updateEvents(newEvent);} );
  }

  removeParticipant(event, _user) {
    event.removeParticipant(_user).subscribe( newEvent => this.updateEvents(newEvent) );
  }

  setCook(event, _user) {
    event.setCook(_user).subscribe( newEvent => this.updateEvents(newEvent) );
  }

  removeCook(event) {
    event.removeCook().subscribe( newEvent => this.updateEvents(newEvent) );
  }

  updateEvents(event) {
    let idx = _.findIndex(this.events, ['date', event.date]);
    
    console.log('Going to update events[' + idx + ']..');

    let newEvent = this.restangular.one('events');
    newEvent.date = event.date;
    newEvent.cook = event.cook;
    newEvent.participants = event.participants;

    this.events[idx] = newEvent;

    this.loadUsers()
  }
}
