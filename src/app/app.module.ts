import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { RestangularModule, Restangular } from 'ngx-restangular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import * as _ from 'lodash';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';


// Function for setting the default restangular configuration
export function RestangularConfigFactory (RestangularProvider) {
  RestangularProvider.setBaseUrl(environment.apiBaseUrl);
  //RestangularProvider.setDefaultHeaders({'Authorization': 'Bearer UDXPx-Xko0w4BRKajozCVy20X11MRZs1'});

  RestangularProvider.extendModel('events', event => { 
    event.getDate = (format) => moment(event.date).format(format);
    event.getRole = (_user) => event.isCook(_user) ? 'cook' : (event.isParticipant(_user) ? 'participant' : 'none')
    event.hasCook = () => !!event.cook;

    event.isCook = (_user) => (event.cook && event.cook._id == _user);
    event.isParticipant = (_user) => _.map(event.participants, '_id').indexOf(_user) > -1;
    
    event.addParticipant = (_user) => event.one(event.date.substring(0, 10)).one('participants', _user).put();
    event.removeParticipant = (_user) => event.one(event.date.substring(0, 10)).one('participants', _user).remove();
    event.setCook = (_user) => event.one(event.date.substring(0, 10)).one('cook', _user).put();
    event.removeCook = () => event.one(event.date.substring(0, 10)).one('cook').remove();
    return event;
  });

  RestangularProvider.addResponseInterceptor( (data, operation, model, url, response) => {
    if (model == 'events' ) {
      // on GET /events we need to format the date of all events
      for (let event of data) {
        event.date = moment(event.date).format('YYYY-MM-DD');
      }
    }

    if ( model == 'participants' || model == 'cook') {
      // on PUT /events/YYYY-MM-DD/(participants|cook)/:_id we need to format the date of just one event
      data.date = moment(data.date).format('YYYY-MM-DD');
    }
    
    if ( model == 'users' ) {
      for (let user of data) {
        user.credits = user.credits || 0;
      }
    }

    return data;
  });
 
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    NgbModule.forRoot(),
  ],
  entryComponents: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
